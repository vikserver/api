# Vikserver REST API

REST API for vikserver. This API is being developed to replace vikserver-backend as it makes use of socket.io, so it's locked with JavaScript and it's hard to extend.
As the first milestone we're trying to achieve is to remove socket.io from vshort and replace it with this API.
