package database;

import (
    "log"
    "os"
    "errors"
    "database/sql"
  . "gitlab.com/vikserver/api/types"
  _ "github.com/go-sql-driver/mysql"
)

var errorLog=log.New(os.Stderr, "Database Error: ", 0);

type MySQLConnection struct{
    db *sql.DB;
}

func CreateMySQLConnection(config *MySQLConfig) *MySQLConnection{
    db, err:=sql.Open("mysql", config.CreateDSN());
    if err != nil {
        panic(err);
    }
    //Test connection
    rows,err:=db.Query("SELECT 1+1 AS result");
    if rows!= nil {
        rows.Close();
    }
    if err != nil {
        panic(err);
    }
    cn:=MySQLConnection{db};
    return &cn;
}

/**
  * Takes a Link ID and returns the associated link
  * Possible http status are
  *    404 - UID not found on database
  *    500 - General server error
  *    0   - No errors ocurred, use the default
 **/
func shortLinkQuery(parameters []string, ch chan Response, conn *MySQLConnection){
    defer recoverWithStatus(ch, 500);
    id:=parameters[0];
    resp:=Response{};
    q:="SELECT link FROM short WHERE uid=? LIMIT 1";
    row:=conn.db.QueryRow(q, id);
    var link string;
    switch err := row.Scan(&link); err{
    case sql.ErrNoRows:
        resp.Error=errors.New("There's no link with that UID");
        resp.HttpStatus=404;
        resp.HasError=true;
    case nil:
        resp.Result=[]string{link};
        resp.HasError=false;
    default:
        errorLog.Print(err);
        resp.Error=err;
        resp.HttpStatus=500;
        resp.HasError=true;
    }
    ch<-resp;
}

/**
  * Resolve a query calling a function with the needed parameters
 **/
func resolveQuery(q *Query, conn *MySQLConnection){
    switch(q.Type){
        case "ShortLinkQuery":{
            shortLinkQuery(q.Parameters, q.Result, conn);
        }
        default:{
            r:=Response{
                HttpStatus: 405,
                HasError: true,
                Error: errors.New("Cannot resolve method"),
            }
            q.Result<-r;
        }
    }
}

/**
  * Start listening on a channel for requests
  *
 **/

func QueryCentral(config *MySQLConfig, query chan *Query, exit chan bool){
    conn:=CreateMySQLConnection(config);
    defer conn.db.Close();
    e:=false;
    for ;!e; {
        select {
            case q:=<-query:{
                resolveQuery(q, conn);
            }
            case ex:=<-exit:{
                e=ex;
                exit<-ex;
            }
        }
    }
}

func recoverWithStatus(mchan chan Response, httpStatus int){
    if e:=recover(); e != nil {
        var err error;
        switch v:=e.(type) {
        case error:
            err=v;
        case string:
            err=errors.New(v);
        }
        errorLog.Print(err);
        resp:=Response{
            HasError: true,
            Error: err,
            HttpStatus: httpStatus,
        }
        select {
        case mchan<-resp:
            return;
        default:
            panic(errors.New("Response channel is closed and/or unavailable"));
        }
    }
}
