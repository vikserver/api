package database;

import(
    "errors"
    "os"
    "testing"
  . "gitlab.com/vikserver/api/types"
)

var (
    mysqlConfig=MySQLConfig{
        User:getFromEnv("MYSQL_USER", ""),
        Password: getFromEnv("MYSQL_PASSWORD", ""),
        Host: getFromEnv("MYSQL_HOST", ""),
        Database: getFromEnv("MYSQL_DATABASE", ""),
    };
    dbconn *MySQLConnection;
)

func TestResolveQuery(t *testing.T){
    asd:=[]struct{query *Query; expectedResults []int}{
        {
            &Query{Type: "InvalidType", Parameters: []string{"a"}, Result: make(chan Response, 1)},
            []int{405},
        },
        {
            &Query{Type: "ShortLinkQuery", Parameters: []string{"a"}, Result: make(chan Response, 1)},
            []int{500}, // Database connection pointer is nil, so query won't work
        },
    }
    for _,test:=range(asd) {
        resolveQuery(test.query, nil);
        result:=<-test.query.Result;
        if status:=result.HttpStatus; intArrayHasInt(&test.expectedResults, status) {
            t.Logf("I: Got status %d for %v\n", result.HttpStatus, test.query.Type);
        } else {
            t.Errorf("E: Got status %d (expected any of %v) for %v", result.HttpStatus, test.expectedResults, test.query.Type);
        }

        if result.HasError!=(result.Error!=nil) {
            t.Errorf("E: status.HasError=%v but status.Error points to another thing: %q", result.HasError, result.Error);
        }
    }
}

func TestCreateMySQLConnection(t *testing.T){
        //At this time, MySQL credentials can be incorrect, as connection is only initialized
    t.Logf("Connecting to database %q on host %q\n", mysqlConfig.Database, mysqlConfig.Host);
    dbconn=CreateMySQLConnection(&mysqlConfig);

    //Now connection must have the right credentials
    _,err:=dbconn.db.Query("SELECT 1+1 AS result");
    if err != nil {
        t.Errorf("Error while querying the server: %s", err);
    } else {
        t.Logf("Connection to database OK");
    }
}

func TestShortLinkQuery(t *testing.T){
    if dbconn == nil {
        TestCreateMySQLConnection(t);
    }
    q:=[]struct{UID string; status int; err bool}{
        {"qwe", 404, true},
        {"asd", 0, false},
        {"asd'; DROP TABLE short", 404, true},
    };
    for _,v:=range(q) {
        params:=[]string{v.UID};
        rsp:=make(chan Response, 1);
        shortLinkQuery(params, rsp, dbconn);
        res:=<-rsp;
        if res.HttpStatus == v.status && res.HasError==v.err {
            if res.HasError != (res.Error != nil){
                t.Error("Response Error field and HasError one differ\n");
            }
            t.Logf("Response recevied with status %d as expected", v.status);
        } else {
            t.Errorf("Response received with status %d but %d was expected", res.HttpStatus, v.status);
        }
    }
}

func TestQueryCentral(t *testing.T){
    ch:=make(chan *Query, 1);
    ech:=make(chan bool, 1);
    queries:=[]struct{query Query; expectedStatus int; errorExpected bool}{
        {
            Query{Type: "InvalidType", Parameters: []string{}, Result: make(chan Response, 1)},
            405, true,
        },
        {
            Query{Type: "InvalidType", Result: make(chan Response, 1)},
            405, true,
        },
        {
            Query{Type: "ShortLinkQuery", Result: make(chan Response, 1)},
            500, true,
        },
        {
            Query{Type: "ShortLinkQuery", Parameters: []string{"inexistent UID"}, Result: make(chan Response, 1)},
            404, true,
        },
        {
            Query{Type: "ShortLinkQuery", Parameters: []string{"asd"}, Result: make(chan Response, 1)},
            0, false,
        },
    }
    go QueryCentral(&mysqlConfig, ch, ech);
    for _,query:=range(queries){
        ch<-&query.query;
        res:=<-query.query.Result;
        if res.HttpStatus != query.expectedStatus {
            t.Errorf("Expected status %d and got %d while processing %v\n", res.HttpStatus, query.expectedStatus, query.query);
        } else if res.HasError != query.errorExpected {
            t.Errorf("Unexpected error: %s\n", res.Error);
        } else {
            t.Logf("Processed correctly %v and got status %d\n", query.query, res.HttpStatus);
        }
    }
    ech<-true;
    <-ech;
}

func TestRecoverWithStatus(t *testing.T){
    panics:=[]bool{true, false}; //Panic with string(false) or with error(true)
    statuses:=[]int{500, 403};
    ch:=make(chan Response, 1);
    for k,p:=range(panics) {
        st:=statuses[k];
        go func(state int, ty bool){
            defer recoverWithStatus(ch, state);
            if ty {
                panic(errors.New("Test error"));
            } else {
                panic("Test error");
            }
        }(st, p);
        if r:=<-ch; r.HttpStatus != st {
            t.Errorf("Recovered from panic with status %d but expected %d\n", r.HttpStatus, st);
        } else if r.HasError != (r.Error != nil) {
            t.Errorf("HasError and Error fields mismatch. Error: %s\n", r.Error);
        } else if !r.HasError {
            t.Error("There must be an error, no error recovered\n");
        } else {
            t.Logf("Recovered from error %q with status %d\n", r.Error, r.HttpStatus);
        }
    }
}

func intArrayHasInt(array *[]int, num int) bool{
    for _,v:=range(*array) {
        if v==num {
            return true;
        }
    }
    return false;
}

func getFromEnv(name, def string) string{
    if r:=os.Getenv(name); r!="" {
        return r;
    }
    return def;
}
