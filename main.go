package main

import (
    "os"
    "gitlab.com/vikserver/api/database"
    "gitlab.com/vikserver/api/rest"
  . "gitlab.com/vikserver/api/types"
)

func main(){
    config:=generateConfig();
    print(config.Format());
    cq:=make(chan *Query, 10);
    ex:=make(chan bool, 1)
    go database.QueryCentral(&config.MySQL, cq, ex);
    rest.CreateServer(&config.HttpConfig, cq, ex);
    // ex<-true;
}

func generateConfig() ConfigurationStruct{
    cnf:=ConfigurationStruct{
        HttpConfig:HttpConfig{
            Port:getFromEnv("PORT","8080"),
        },
        MySQL:MySQLConfig{
            User:getFromEnv("MYSQL_USER", ""),
            Password: getFromEnv("MYSQL_PASSWORD", ""),
            Host: getFromEnv("MYSQL_HOST", ""),
            Database: getFromEnv("MYSQL_DATABASE", ""),
        },
        OpenPGPKeys:OpenPGPKeyStruct{
            Passphrase: getFromEnv("OPENPGP_PASSPHRASE", ""),
        },
    };
    return cnf;
}

func getFromEnv(name, def string) string{
    if val:=os.Getenv(name); val!="" {
        return val;
    }
    return def;
}

