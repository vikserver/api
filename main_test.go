package main;

import (
    "os"
    "testing"
)

func TestGenerateConfig(t *testing.T){
    config:=generateConfig();
    if config.Port == "" {
        t.Errorf("Invalid port number %s\n", config.Port);
    } else {
        t.Logf("Correct config struct %v\n", config);
    }
}

func TestGetFromEnv(t *testing.T){
    vars:=[]struct{name, value string}{
        {
            name: "MYSQL_USER",
            value: "nothing",
        },
        {
            name: "INEXISTENT_VARIABLE",
            value: "default value",
        },
    }
    for _,v:=range(vars) {
        r:=getFromEnv(v.name, v.value);
        if r != v.value && os.Getenv(v.name) == "" {
            t.Errorf("Unexpected value %q, expecting %q", r, v.value);
        }else{
            t.Logf("Value %q OK", r);
        }
    }
}

