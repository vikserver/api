package responses;

import (
    "encoding/json"
)

type RESTResponse interface{
    ToJSON() ([]byte, error);
}

type GetShortLink struct{
    Short, Error string;
}

func NewGetShortLink(Link, Error string) GetShortLink{
    return GetShortLink{Link, Error};
}

func (st GetShortLink) ToJSON() ([]byte,error){
    return json.Marshal(st);
}

