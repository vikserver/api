package rest;

import (
    "fmt"
    "strings"
    "errors"
    "net/http"
    "gitlab.com/vikserver/api/responses"
  . "gitlab.com/vikserver/api/types"
)

var (
    dbchannel chan *Query;
)

func CreateServer(config *HttpConfig, db chan *Query, ex chan bool){
    dbchannel=db;
    registerServices();
    http.HandleFunc("/test", func(w http.ResponseWriter, _ *http.Request){
        fmt.Fprintf(w, "Testing some things like this. %v\n", config.Port);
    });
    err:=http.ListenAndServe(fmt.Sprintf(":%s", config.Port), nil);
    if err != nil {
        panic(err);
    }
}

func registerServices(){
    s0:=http.RedirectHandler("https://viksrv.tk", http.StatusFound); //Default redirect
    s1:=func(writter http.ResponseWriter, req *http.Request){ //ShortLink redirect
        writter.Header().Add("Access-Control-Allow-Origin", "*");
        res,status:=getShortLink(req.URL.Path);
        if status == 0 {
            writter.Header().Add("Location", res);
            writter.WriteHeader(http.StatusFound);
        } else {
            writter.WriteHeader(status);
            fmt.Fprintln(writter, res);
        }
    }
    s2:=func(writter http.ResponseWriter, req *http.Request){
    writter.Header().Add("Access-Control-Allow-Origin", "*");
        short,status:=getShortLink(req.URL.Path);
        var shlink responses.RESTResponse;
        if status != 0 {
            writter.WriteHeader(status);
            shlink=responses.NewGetShortLink("", short);
        } else {
            shlink=responses.NewGetShortLink(short, "");
        }
        marshal,err:=shlink.ToJSON();
        if err != nil {
            writter.WriteHeader(http.StatusInternalServerError);
        } else {
            writter.Write(marshal);
        }
    }
    http.Handle("/", s0);
    http.HandleFunc("/s/", s1);
    http.HandleFunc("/get-link/", s2);
}

func buildQueryForShortLink(path string) (*Query,error){
    spl:=strings.Split(path, "/");
    if len(spl) != 3 {
        return nil, errors.New("Invalid URL length");
    }
    id:=spl[2];
    if len(id) != 6 {
        return nil, errors.New("Invalid UID length");
    }
    q:=Query{
        Type: "ShortLinkQuery",
        Parameters: []string{id},
        Result: make(chan Response, 1),
    };
    return &q, nil;
}

func getShortLink(path string) (string,int){
    var (
        result string;
        status int;
    )
    query,err:=buildQueryForShortLink(path);
    if err != nil {
        status=http.StatusBadRequest;
        result=err.Error();
        return result,status;
    }
    dbchannel<-query;
    resp:=<-query.Result;
    if resp.HasError {
        if resp.HttpStatus != 0 {
            status=resp.HttpStatus;
        } else {
            status=http.StatusBadRequest;
        }
        if resp.HttpStatus <500 {
            result=resp.Error.Error();
        }
    } else {
        result=resp.Result[0];
    }
    return result,status;
}

