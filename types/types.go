package types

import (
    "fmt"
    "strings"
)

type ConfigurationStruct struct{
    HttpConfig;
    MySQL MySQLConfig;
    OpenPGPKeys OpenPGPKeyStruct;
}

type MySQLConfig struct{
    User, Password, Host, Database string;
}

type OpenPGPKeyStruct struct{
    PubKey, PrivKey []byte;
    Passphrase string;
}

type HttpConfig struct{
    Port string;
}

func (st ConfigurationStruct) Format() string{
    return fmt.Sprintf("Port: %s\nMySQL Config: \n%s\n",st.Port, st.MySQL.CreateDSN());
}

func (st MySQLConfig) CreateDSN() string{
    var net string;
    if strings.HasPrefix(st.Host, "/"){
        net="unix";
    }
    return fmt.Sprintf("%s:%s@%s(%s)/%s", st.User, st.Password, net, st.Host, st.Database);
}

type Query struct{
    Type string;
    Parameters []string;
    Result chan Response;
}

type Response struct{
    Error error;
    HasError bool;
    HttpStatus int;
    Result []string;
}

