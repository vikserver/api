package types;

import (
    "testing"
)

func TestCreateDSN(t *testing.T){
    configs:=[]struct{expected string; config MySQLConfig}{
        {
            expected: "myuser:mypassword@(127.0.0.1)/someDatabase",
            config: MySQLConfig{
                User: "myuser",
                Password: "mypassword",
                Host: "127.0.0.1",
                Database: "someDatabase",
            },
        },
        {
            expected: "myuser:mypassword@unix(/run/mysqld/mysqld.sock)/someDatabase",
            config: MySQLConfig{
                User: "myuser",
                Password: "mypassword",
                Host: "/run/mysqld/mysqld.sock",
                Database: "someDatabase",
            },
        },
    };
    for _,config:=range(configs) {
        dsn:=config.config.CreateDSN();
        if dsn != config.expected {
            t.Errorf("%q expected but got %q\n", config.expected, dsn);
        } else {
            t.Logf("Received %q as expected\n", dsn);
        }
    }
}

func TestFormat(t *testing.T){
    config:=ConfigurationStruct{};
    f:=config.Format();
    if f != "" {
        t.Logf("Got correct return from Format with empty config: %s\n", f);
    } else {
        t.Error("Format output cannot be empty!");
    }
}
